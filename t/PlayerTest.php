<?php
require_once 'Tournament/Player.php';
class TestPlayer extends PHPUnit_Framework_TestCase {
    protected $p;

    public function setUp() {
        $this->p = new Player(1);
    }

    public function testHandle() {
        $this->p->setHandle('nickname');
        $this->assertEquals('nickname', $this->p->getDisplayName());
        
        try {
            $this->p->setHandle('nick,name');
        }
        catch ( Exception $expected ) {
            $this->assertEquals('Illegal character in handle: ,', $expected->getMessage());
        }
        try {
            $this->p->setHandle('nick|name');
        }
        catch ( Exception $expected ) {
            $this->assertEquals('Illegal character in handle: |', $expected->getMessage());
        }
    }

    public function testName() {
        $this->p->setName('firstname lastname');
        $this->assertEquals('firstname lastname', $this->p->getDisplayName());
        
        try {
            $this->p->setName('firstname,lastname');
        }
        catch ( Exception $expected ) {
            $this->assertEquals('Illegal character in name: ,', $expected->getMessage());
        }
        try {
            $this->p->setName('firstname|lastname');
        }
        catch ( Exception $expected ) {
            $this->assertEquals('Illegal character in name: |', $expected->getMessage());
        }
    }

}
