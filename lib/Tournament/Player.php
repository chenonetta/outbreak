<?php
class Player {
    protected $handle;
    protected $name;
    protected $id;
    protected $phone;
    protected $email;
    protected $barcode; //not used
    
    public function __construct($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
    public function setHandle($h) {
        if ( preg_match('/(,|\|)/', $h, $matches) ) {
            throw new Exception('Illegal character in handle: ' . $matches[0]);
        }
        $this->handle = $h;
    }
    public function setName($n) {
        if ( preg_match('/(,|\|)/', $n, $matches) ) {
            throw new Exception('Illegal character in name: ' . $matches[0]);
        }
        $this->name = $n;
    }
    public function getDisplayName() {
        $displayname = isset($this->handle) ? $this->handle : ( isset($this->name) ? $this->name : 'TBA' );
        return $displayname;
    }
}